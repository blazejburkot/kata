package kata.common;

import java.net.URI;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class JavaNioResourceReader {

    public Stream<String> readLines(final String resourceLocation)  {
        return readLines(resourceLocation, StandardCharsets.UTF_8);
    }

    public Stream<String> readLines(final String resourceLocation, final Charset resourceEncoding) {
        try {
            final URI resourceLocationUri = getClass().getClassLoader().getResource(resourceLocation).toURI();
            final Path path = Paths.get(resourceLocationUri);
            return Files.lines(path, resourceEncoding);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
