package kata.datamunging.part1;

public class WeatherDataParser {

    public WeatherDataItem parse(final String line) {
        final String[] lineSlitted = line.split("\\s+");

        return WeatherDataItem.builder()
                .dayNumber(Integer.parseInt(lineSlitted[0]))
                .maxTemperature(parseTemperature(lineSlitted[1]))
                .minTemperature(parseTemperature(lineSlitted[2]))
                .build();
    }

    private int parseTemperature(final String temp) {
        return Integer.parseInt(temp.replaceAll("\\D", ""));
    }
}
