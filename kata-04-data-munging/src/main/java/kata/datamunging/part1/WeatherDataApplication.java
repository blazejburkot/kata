package kata.datamunging.part1;

import java.util.Comparator;

@SuppressWarnings("checkstyle:hideutilityclassconstructor")
public class WeatherDataApplication {
    private static final String WEATHER_DATA_LOCATION = "weather.dat";

    public static void main(final String[] args) {
        final WeatherDataReader weatherDataReader = new WeatherDataReader();
        final Integer dayNumberWithMinimalSpread = weatherDataReader
                .readWeatherData(WEATHER_DATA_LOCATION)
                .min(Comparator.comparingInt(WeatherDataItem::getTemperatureSpread))
                .map(WeatherDataItem::getDayNumber)
                .orElseThrow(() -> new IllegalArgumentException("Wrong data"));
        System.out.println(dayNumberWithMinimalSpread);
    }
}
