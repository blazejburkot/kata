package kata.datamunging.part1;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class WeatherDataItem {
    private final int dayNumber;
    private final int maxTemperature;
    private final int minTemperature;

    public int getTemperatureSpread() {
        return maxTemperature - minTemperature;
    }
}
