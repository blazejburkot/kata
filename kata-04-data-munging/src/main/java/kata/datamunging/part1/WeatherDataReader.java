package kata.datamunging.part1;

import kata.common.JavaNioResourceReader;
import org.springframework.util.StringUtils;

import java.util.stream.Stream;

public class WeatherDataReader {
    private final WeatherDataParser parser;
    private final JavaNioResourceReader resourceReader;

    public WeatherDataReader() {
        this.parser = new WeatherDataParser();
        this.resourceReader = new JavaNioResourceReader();
    }

    public Stream<WeatherDataItem> readWeatherData(final String dataLocation) {
        return resourceReader.readLines(dataLocation)
                .map(String::trim)
                .filter(this::startWithDayNumber)
                .map(parser::parse);
    }

    private boolean startWithDayNumber(final String line) {
        return StringUtils.hasText(line) && Character.isDigit(line.charAt(0));
    }
}
