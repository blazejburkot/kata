package kata.datamunging.part3;

import java.util.Comparator;

@SuppressWarnings("checkstyle:hideutilityclassconstructor")
public class CommonDataApplication {

    public static void main(final String[] args) {
        if (args.length != 1) {
            System.out.println("Usage java -jar artifact.jar <dataType: 'weather' or 'football'>");
            System.exit(-1);
        }

        final DataType dataType = DataType.fromValue(args[0]);
        final CommonDataReader dataReader = new CommonDataReader(dataType);
        final String id = dataReader
                .readData()
                .min(Comparator.comparingInt(CommonDataItem::getDifference))
                .map(CommonDataItem::getId)
                .orElseThrow(() -> new IllegalArgumentException("Wrong data"));
        System.out.println(id);
    }
}
