package kata.datamunging.part3;

import lombok.Getter;

import org.springframework.util.StringUtils;

@Getter
public enum DataType {
    FOOTBALL("football.dat", "^\\s*(?<num>\\d+)\\.\\s+(?<id>\\w+)\\s+(?<P>\\d{1,2})\\s+(?<W>\\d{1,2})\\s+(?<L>\\d{1,2})\\s+(?<D>\\d{1,2})"
            + "\\s+(?<higher>\\d{1,2})\\s+-\\s+(?<lower>\\d{1,2})\\s+(?<Pts>\\d{1,2})$"),
    WEATHER("weather.dat", "^\\s*(?<id>\\d+)\\s+(?<higher>\\d+)\\*?\\s+(?<lower>\\d+)\\*?\\s+(?<restOfData>.+)$");

    private final String dataLocation;
    private final String dataRegex;

    DataType(final String dataLocation, final String dataRegex) {
        this.dataLocation = dataLocation;
        this.dataRegex = dataRegex;
    }

    public static DataType fromValue(final String value) {
        if (StringUtils.hasText(value)) {
            for (DataType dataType : values()) {
                if (dataType.name().equalsIgnoreCase(value)) {
                    return dataType;
                }
            }
        }
        throw new IllegalArgumentException("Unknown dataType: " + value);
    }
}
