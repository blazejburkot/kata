package kata.datamunging.part3;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class CommonDataItem {
    private final String id;
    private final int higherValue;
    private final int lowerValue;

    public int getDifference() {
        return higherValue - lowerValue;
    }
}
