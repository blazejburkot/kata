package kata.datamunging.part3;

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CommonDataParser {
    private final Pattern dataPattern;

    public CommonDataParser(final String dataRegex) {
        this.dataPattern = Pattern.compile(dataRegex);
    }

    public Optional<CommonDataItem> parse(final String line) {
        final Matcher matcher = dataPattern.matcher(line);
        if (!matcher.matches()) {
            return Optional.empty();
        }

        final String id = matcher.group("id");
        final String higherValue = matcher.group("higher");
        final String lowerValue = matcher.group("lower");

        return Optional.of(CommonDataItem.builder()
                .id(id)
                .higherValue(Integer.parseInt(higherValue))
                .lowerValue(Integer.parseInt(lowerValue))
                .build());
    }
}
