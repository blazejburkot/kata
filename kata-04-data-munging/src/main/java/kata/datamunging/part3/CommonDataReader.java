package kata.datamunging.part3;

import kata.common.JavaNioResourceReader;

import java.util.Optional;
import java.util.stream.Stream;

public class CommonDataReader {
    private final CommonDataParser parser;
    private final JavaNioResourceReader resourceReader;
    private final DataType dataType;

    public CommonDataReader(final DataType dataType) {
        this.dataType = dataType;
        this.parser = new CommonDataParser(dataType.getDataRegex());
        this.resourceReader = new JavaNioResourceReader();
    }

    public Stream<CommonDataItem> readData() {
        return resourceReader.readLines(dataType.getDataLocation())
                .map(parser::parse)
                .filter(Optional::isPresent)
                .map(Optional::get);
    }
}
