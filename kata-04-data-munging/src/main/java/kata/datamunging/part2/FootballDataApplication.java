package kata.datamunging.part2;

import java.util.Comparator;

@SuppressWarnings("checkstyle:hideutilityclassconstructor")
public class FootballDataApplication {
    private static final String FOOTBALL_DATA_LOCATION = "football.dat";

    public static void main(final String[] args) {
        final FootballDataReader footballDataReader = new FootballDataReader();
        final String teamName = footballDataReader
                .readFootballData(FOOTBALL_DATA_LOCATION)
                .min(Comparator.comparingInt(FootballDataItem::getGoalDifference))
                .map(FootballDataItem::getTeamName)
                .orElseThrow(() -> new IllegalArgumentException("Wrong data"));
        System.out.println(teamName);
    }
}
