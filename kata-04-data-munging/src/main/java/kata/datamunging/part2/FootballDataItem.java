package kata.datamunging.part2;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class FootballDataItem {
    private final String teamName;
    private final int goalsScored;
    private final int goalsLost;

    public int getGoalDifference() {
        return goalsScored - goalsLost;
    }
}
