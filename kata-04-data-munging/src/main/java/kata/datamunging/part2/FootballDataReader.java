package kata.datamunging.part2;

import kata.common.JavaNioResourceReader;
import org.springframework.util.StringUtils;

import java.util.stream.Stream;

public class FootballDataReader {
    private final FootballDataParser parser;
    private final JavaNioResourceReader resourceReader;

    public FootballDataReader() {
        this.parser = new FootballDataParser();
        this.resourceReader = new JavaNioResourceReader();
    }

    public Stream<FootballDataItem> readFootballData(final String dataLocation) {
        return resourceReader.readLines(dataLocation)
                .map(String::trim)
                .filter(this::startWithDayNumber)
                .map(parser::parse);
    }

    private boolean startWithDayNumber(final String line) {
        return StringUtils.hasText(line) && Character.isDigit(line.charAt(0));
    }
}
