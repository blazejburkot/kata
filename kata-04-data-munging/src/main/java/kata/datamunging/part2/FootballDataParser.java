package kata.datamunging.part2;

public class FootballDataParser {
    private static final int GOALS_SCORED_CELL_IDX = 6;
    private static final int GOALS_LOST_CELL_IDX = 8;

    public FootballDataItem parse(final String line) {
        final String[] lineSlitted = line.split("\\s+");

        return FootballDataItem.builder()
                .teamName(lineSlitted[1])
                .goalsScored(Integer.parseInt(lineSlitted[GOALS_SCORED_CELL_IDX]))
                .goalsLost(Integer.parseInt(lineSlitted[GOALS_LOST_CELL_IDX]))
                .build();
    }
}
