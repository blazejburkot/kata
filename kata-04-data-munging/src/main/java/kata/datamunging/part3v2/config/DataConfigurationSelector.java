package kata.datamunging.part3v2.config;

import java.util.HashMap;
import java.util.Map;

public class DataConfigurationSelector {
    private final Map<String, DataAppConfiguration> configs;

    public DataConfigurationSelector() {
        this.configs = new HashMap<>();
        this.configs.put("weather", new WeatherDataConfiguration());
        this.configs.put("football", new FootballDataConfiguration());
    }

    public DataAppConfiguration getDataAppConfiguration(final String key) {
        return configs.get(key.toLowerCase());
    }
}
