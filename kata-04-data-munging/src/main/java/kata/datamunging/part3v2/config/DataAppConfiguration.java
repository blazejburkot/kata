package kata.datamunging.part3v2.config;

import kata.datamunging.part3v2.DataReader;
import kata.datamunging.part3v2.model.DataItem;

import java.util.Comparator;

public interface DataAppConfiguration {

    DataReader<? extends DataItem> dataReader();

    Comparator<DataItem> comparator();
}
