package kata.datamunging.part3v2.config;

import kata.datamunging.part3v2.DataReader;
import kata.datamunging.part3v2.comparator.TemperatureSpreadWeatherComparator;
import kata.datamunging.part3v2.model.WeatherDataItem;
import kata.datamunging.part3v2.parser.DataParser;
import kata.datamunging.part3v2.parser.WeatherDataParser;

public class WeatherDataConfiguration implements DataAppConfiguration {
    private static final String DATA_LOCATION = "weather.dat";

    @Override
    public DataReader<WeatherDataItem> dataReader() {
        final DataParser<WeatherDataItem> parser = new WeatherDataParser();
        return new DataReader<>(parser, DATA_LOCATION);
    }

    @Override
    public TemperatureSpreadWeatherComparator comparator() {
        return new TemperatureSpreadWeatherComparator();
    }
}
