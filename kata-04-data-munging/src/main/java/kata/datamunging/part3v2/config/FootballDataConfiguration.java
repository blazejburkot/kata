package kata.datamunging.part3v2.config;

import kata.datamunging.part3v2.DataReader;
import kata.datamunging.part3v2.comparator.GoalDifferenceFootballComparator;
import kata.datamunging.part3v2.model.FootballDataItem;
import kata.datamunging.part3v2.parser.DataParser;
import kata.datamunging.part3v2.parser.FootballDataParser;

public class FootballDataConfiguration implements DataAppConfiguration {
    private static final String DATA_LOCATION = "football.dat";

    @Override
    public DataReader<FootballDataItem> dataReader() {
        final DataParser<FootballDataItem> parser = new FootballDataParser();
        return new DataReader<>(parser, DATA_LOCATION);
    }

    @Override
    public GoalDifferenceFootballComparator comparator() {
        return new GoalDifferenceFootballComparator();
    }
}
