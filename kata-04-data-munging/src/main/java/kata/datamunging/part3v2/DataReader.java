package kata.datamunging.part3v2;

import kata.common.JavaNioResourceReader;
import kata.datamunging.part3v2.model.DataItem;
import kata.datamunging.part3v2.parser.DataParser;

import java.util.Optional;
import java.util.stream.Stream;

public class DataReader<T extends DataItem> {
    private final DataParser<T> parser;
    private final JavaNioResourceReader resourceReader;
    private final String dataLocation;

    public DataReader(final DataParser<T> parser, final String dataLocation) {
        this.parser = parser;
        this.dataLocation = dataLocation;
        this.resourceReader = new JavaNioResourceReader();
    }

    public Stream<T> readData() {
        return resourceReader.readLines(dataLocation)
                .map(parser::parse)
                .filter(Optional::isPresent)
                .map(Optional::get);
    }
}
