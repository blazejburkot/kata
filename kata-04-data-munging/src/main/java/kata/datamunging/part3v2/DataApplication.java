package kata.datamunging.part3v2;

import kata.datamunging.part3v2.config.DataAppConfiguration;
import kata.datamunging.part3v2.config.DataConfigurationSelector;
import kata.datamunging.part3v2.model.DataItem;

import java.util.Comparator;

@SuppressWarnings("checkstyle:hideutilityclassconstructor")
public class DataApplication {

    public static void main(final String[] args) {
        if (args.length != 1) {
            System.out.println("Usage java -jar artifact.jar <dataType: 'weather' or 'football'>");
            System.exit(-1);
        }

        final DataConfigurationSelector configsSelector = new DataConfigurationSelector();
        final DataAppConfiguration config = configsSelector.getDataAppConfiguration(args[0]);
        final DataReader<? extends DataItem> dataReader = config.dataReader();
        final Comparator<DataItem> comparator = config.comparator();

        final String id = dataReader
                .readData()
                .min(comparator)
                .map(DataItem::getId)
                .orElseThrow(() -> new IllegalArgumentException("Wrong data"));
        System.out.println(id);
    }
}
