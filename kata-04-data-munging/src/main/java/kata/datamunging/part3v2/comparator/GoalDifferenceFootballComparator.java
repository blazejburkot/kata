package kata.datamunging.part3v2.comparator;

import kata.datamunging.part3v2.model.DataItem;
import kata.datamunging.part3v2.model.FootballDataItem;

import java.util.Comparator;

public class GoalDifferenceFootballComparator implements Comparator<DataItem> {

    @Override
    public int compare(final DataItem o1, final DataItem o2) {
        return ((FootballDataItem) o1).getGoalDifference() - ((FootballDataItem) o2).getGoalDifference();
    }
}
