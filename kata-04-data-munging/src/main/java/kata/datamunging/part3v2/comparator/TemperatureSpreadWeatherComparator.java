package kata.datamunging.part3v2.comparator;

import kata.datamunging.part3v2.model.DataItem;
import kata.datamunging.part3v2.model.WeatherDataItem;

import java.util.Comparator;

public class TemperatureSpreadWeatherComparator implements Comparator<DataItem> {

    @Override
    public int compare(final DataItem o1, final DataItem o2) {
        return ((WeatherDataItem) o1).getTemperatureSpread() - ((WeatherDataItem) o2).getTemperatureSpread();
    }
}
