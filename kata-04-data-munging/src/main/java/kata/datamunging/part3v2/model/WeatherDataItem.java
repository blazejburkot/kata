package kata.datamunging.part3v2.model;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class WeatherDataItem implements DataItem {
    private final int dayNumber;
    private final int maxTemperature;
    private final int minTemperature;

    public int getTemperatureSpread() {
        return maxTemperature - minTemperature;
    }

    @Override
    public String getId() {
        return Integer.toString(dayNumber);
    }
}
