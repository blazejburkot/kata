package kata.datamunging.part3v2.model;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class FootballDataItem implements DataItem{
    private final String teamName;
    private final int goalsScored;
    private final int goalsLost;

    public int getGoalDifference() {
        return goalsScored - goalsLost;
    }

    @Override
    public String getId() {
        return teamName;
    }
}
