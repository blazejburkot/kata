package kata.datamunging.part3v2.parser;

import kata.datamunging.part3v2.model.WeatherDataItem;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WeatherDataParser extends AbstractDataParser<WeatherDataItem> {
    private static final Pattern DATA_ITEM_REGEX
            = Pattern.compile("^\\s*(?<dy>\\d+)\\s+(?<MxT>\\d+)\\*?\\s+(?<MnT>\\d+)\\*?\\s+(?<restOfData>.+)$");

    public WeatherDataParser() {
        super(DATA_ITEM_REGEX);
    }

    @Override
    protected WeatherDataItem mapToDataItem(final Matcher matcher) {
        return WeatherDataItem.builder()
                .dayNumber(Integer.parseInt(matcher.group("dy")))
                .maxTemperature(Integer.parseInt(matcher.group("MxT")))
                .minTemperature(Integer.parseInt(matcher.group("MnT")))
                .build();
    }
}
