package kata.datamunging.part3v2.parser;

import kata.datamunging.part3v2.model.FootballDataItem;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FootballDataParser extends AbstractDataParser<FootballDataItem> {
    private static final Pattern DATA_ITEM_REGEX = Pattern.compile("^\\s*(?<num>\\d+)\\.\\s+(?<Team>\\w+)\\s+(?<P>\\d{1,2})\\s+"
            + "(?<W>\\d{1,2})\\s+(?<L>\\d{1,2})\\s+(?<D>\\d{1,2})\\s+(?<F>\\d{1,2})\\s+-\\s+(?<A>\\d{1,2})\\s+(?<Pts>\\d{1,2})$");

    public FootballDataParser() {
        super(DATA_ITEM_REGEX);
    }

    @Override
    protected FootballDataItem mapToDataItem(final Matcher matcher) {
        return FootballDataItem.builder()
                .teamName(matcher.group("Team"))
                .goalsScored(Integer.parseInt(matcher.group("F")))
                .goalsLost(Integer.parseInt(matcher.group("A")))
                .build();
    }
}
