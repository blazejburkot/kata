package kata.datamunging.part3v2.parser;

import kata.datamunging.part3v2.model.DataItem;

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class AbstractDataParser<T extends DataItem> implements DataParser<T> {
    private final Pattern dataItemPattern;

    public AbstractDataParser(final Pattern dataItemPattern) {
        this.dataItemPattern = dataItemPattern;
    }

    @Override
    public Optional<T> parse(final String line) {
        final Matcher matcher = dataItemPattern.matcher(line);
        return matcher.matches()
                ? Optional.of(mapToDataItem(matcher))
                : Optional.empty();
    }

    protected abstract T mapToDataItem(final Matcher matcher);
}
