package kata.datamunging.part3v2.parser;

import kata.datamunging.part3v2.model.DataItem;

import java.util.Optional;

public interface DataParser<T extends DataItem> {

    Optional<T> parse(String line);
}
