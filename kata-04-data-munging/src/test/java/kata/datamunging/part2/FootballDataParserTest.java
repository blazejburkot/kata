package kata.datamunging.part2;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class FootballDataParserTest {
    private FootballDataParser parser = new FootballDataParser();

    @Test
    public void shouldParseWeatherData_1() {
        // given
        final String line = "1. Arsenal         38    26   9   3    79  -  36    87";
        // when
        final FootballDataItem footballDataItem = parser.parse(line);
        // then
        assertThat(footballDataItem.getTeamName()).isEqualTo("Arsenal");
        assertThat(footballDataItem.getGoalsScored()).isEqualTo(79);
        assertThat(footballDataItem.getGoalsLost()).isEqualTo(36);
        assertThat(footballDataItem.getGoalDifference()).isEqualTo(79 - 36);
    }
}