package kata.datamunging.part3v2.parser;

import kata.datamunging.part3v2.model.FootballDataItem;

import org.junit.Test;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

public class FootballDataParserTest {
    private FootballDataParser parser = new FootballDataParser();

    @Test
    public void shouldParseFootballData() {
        // given
        final String line = "1. Arsenal         38    26   9   3    79  -  36    87";
        // when
        final FootballDataItem footballDataEntry = parser.parse(line).get();
        // then
        assertThat(footballDataEntry.getId()).isEqualTo("Arsenal");
        assertThat(footballDataEntry.getGoalsScored()).isEqualTo(79);
        assertThat(footballDataEntry.getGoalsLost()).isEqualTo(36);
        assertThat(footballDataEntry.getGoalDifference()).isEqualTo(79 - 36);
    }

    @Test
    public void shouldNotParseFootballData() {
        // given
        final String line = "       Team            P     W    L   D    F      A     Pts";
        // when
        final Optional<FootballDataItem> footballDataEntry = parser.parse(line);
        // then
        assertThat(footballDataEntry.isPresent()).isFalse();
    }
}