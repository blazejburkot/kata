package kata.datamunging.part3v2.parser;

import kata.datamunging.part3v2.model.WeatherDataItem;

import org.junit.Test;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

public class WeatherDataParserTest {
    private WeatherDataParser parser = new WeatherDataParser();

    @Test
    public void shouldParseWeatherData_1() {
        // given
        final String line = "2  79    63    71          46.5       0.00         330  8.7 340  23  3.3  70 28 1004.5";
        // when
        final WeatherDataItem weatherDataItem = parser.parse(line).get();
        // then
        assertThat(weatherDataItem.getDayNumber()).isEqualTo(2);
        assertThat(weatherDataItem.getMaxTemperature()).isEqualTo(79);
        assertThat(weatherDataItem.getMinTemperature()).isEqualTo(63);
        assertThat(weatherDataItem.getTemperatureSpread()).isEqualTo(79 - 63);
    }

    @Test
    public void shouldParseWeatherData_2() {
        // given
        final String line = "9  86    32*   59       6  61.5       0.00         240  7.6 220  12  6.0  78 46 1018.6";
        // when
        final WeatherDataItem weatherDataItem = parser.parse(line).get();
        // then
        assertThat(weatherDataItem.getDayNumber()).isEqualTo(9);
        assertThat(weatherDataItem.getMaxTemperature()).isEqualTo(86);
        assertThat(weatherDataItem.getMinTemperature()).isEqualTo(32);
        assertThat(weatherDataItem.getTemperatureSpread()).isEqualTo(86 - 32);
    }

    @Test
    public void shouldNotParseWeatherData() {
        // given
        final String line = "  mo  82.9  60.5  71.7    16  58.8       0.00              6.9          5.3";
        // when
        final Optional<WeatherDataItem> weatherDataEntry = parser.parse(line);
        // then
        assertThat(weatherDataEntry.isPresent()).isFalse();
    }
}