package kata.datamunging.part3;

import org.junit.Test;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

public class CommonDataParserTest {
    private CommonDataParser footballDataParser = new CommonDataParser(DataType.FOOTBALL.getDataRegex());
    private CommonDataParser weatherDataParser = new CommonDataParser(DataType.WEATHER.getDataRegex());

    @Test
    public void shouldParseWeatherData() {
        // given
        final String line = "   9  86    32*   59       6  61.5       0.00         240  7.6 220  12  6.0  78 46 1018.6";
        // when
        final CommonDataItem weatherDataEntry = weatherDataParser.parse(line).get();
        // then
        assertThat(weatherDataEntry.getId()).isEqualTo("9");
        assertThat(weatherDataEntry.getHigherValue()).isEqualTo(86);
        assertThat(weatherDataEntry.getLowerValue()).isEqualTo(32);
        assertThat(weatherDataEntry.getDifference()).isEqualTo(86 - 32);
    }

    @Test
    public void shouldNotParseWeatherData() {
        // given
        final String line = "  mo  82.9  60.5  71.7    16  58.8       0.00              6.9          5.3";
        // when
        final Optional<CommonDataItem> weatherDataEntry = weatherDataParser.parse(line);
        // then
        assertThat(weatherDataEntry.isPresent()).isFalse();
    }

    @Test
    public void shouldParseFootballData() {
        // given
        final String line = "1. Arsenal         38    26   9   3    79  -  36    87";
        // when
        final CommonDataItem footballDataEntry = footballDataParser.parse(line).get();
        // then
        assertThat(footballDataEntry.getId()).isEqualTo("Arsenal");
        assertThat(footballDataEntry.getHigherValue()).isEqualTo(79);
        assertThat(footballDataEntry.getLowerValue()).isEqualTo(36);
        assertThat(footballDataEntry.getDifference()).isEqualTo(79 - 36);
    }

    @Test
    public void shouldNotParseFootballData() {
        // given
        final String line = "       Team            P     W    L   D    F      A     Pts";
        // when
        final Optional<CommonDataItem> footballDataEntry = footballDataParser.parse(line);
        // then
        assertThat(footballDataEntry.isPresent()).isFalse();
    }
}