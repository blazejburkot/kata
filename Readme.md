# Kata
This project contains solutions to [Kata's puzzles](http://codekata.com/)

## Solution to Kata 2: Karate Chop. [(link)](kata-02-karate-chop/Readme.md)
Implement a binary chop (precisely binary search) in different ways.

## Solution to Kata 4: Data Munging. [(link)](kata-04-data-munging/Readme.md)

## Solution to Kata 19: Word chains. [(link)](kata-19-word-chains-puzzles/Readme.md)
Write a program that solves word chain puzzles (cat → cot → dot → dog).