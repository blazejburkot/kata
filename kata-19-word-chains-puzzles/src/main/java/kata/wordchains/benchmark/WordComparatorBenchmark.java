package kata.wordchains.benchmark;

import kata.wordchains.word.comparator.WordEfficientComparator;
import kata.wordchains.word.comparator.WordOvercomplicatedComparator;
import kata.wordchains.word.comparator.WordSimpleComparator;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Warmup;
import org.openjdk.jmh.infra.Blackhole;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.util.concurrent.TimeUnit;

/*
 * You can run this benchmark:
 *
 * a) Via the command line:
 *    $ mvn -Pbenchmarks clean package
 *    $ java -jar target/benchmarks.jar kata.wordchains.benchmark.WordComparatorBenchmark
 *
 * b) Via the Java API:
 *    just run main method
 */
@Fork(2)
@Warmup(iterations = 1, time = 1)
@Measurement(iterations = 10, time = 1)
@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
public class WordComparatorBenchmark {
    private static final WordEfficientComparator wordEfficientComparator = new WordEfficientComparator();
    private static final WordOvercomplicatedComparator wordOvercomplicatedComparator = new WordOvercomplicatedComparator();
    private static final WordSimpleComparator wordSimpleComparator = new WordSimpleComparator();

    @State(Scope.Benchmark)
    public static class PositiveCase {
        volatile String word1 = "abcdefg";
        volatile String word2 = "abcqefg";
    }

    @State(Scope.Benchmark)
    public static class NegativeCase {
        volatile String word1 = "abcdefg";
        volatile String word2 = "abcqexg";
    }


    @Benchmark
    public void wordEfficientComparator_measurePositiveCaseExecution(final Blackhole blackhole, final PositiveCase positiveCase) {
        blackhole.consume(wordEfficientComparator.areDifferentByOneLetter(positiveCase.word1, positiveCase.word2));
    }
    @Benchmark
    public void wordEfficientComparator_measureNegativeCaseExecution(final Blackhole blackhole, final NegativeCase negativeCase) {
        blackhole.consume(wordEfficientComparator.areDifferentByOneLetter(negativeCase.word1, negativeCase.word2));
    }

    @Benchmark
    public void wordOvercomplicatedComparator_measurePositiveCaseExecution(final Blackhole blackhole, final PositiveCase positiveCase) {
        blackhole.consume(wordOvercomplicatedComparator.areDifferentByOneLetter(positiveCase.word1, positiveCase.word2));
    }
    @Benchmark
    public void wordOvercomplicatedComparator_measureNegativeCaseExecution(final Blackhole blackhole, final NegativeCase negativeCase) {
        blackhole.consume(wordOvercomplicatedComparator.areDifferentByOneLetter(negativeCase.word1, negativeCase.word2));
    }

    @Benchmark
    public void wordSimpleComparator_measurePositiveCaseExecution(final Blackhole blackhole, final PositiveCase positiveCase) {
        blackhole.consume(wordSimpleComparator.areDifferentByOneLetter(positiveCase.word1, positiveCase.word2));
    }
    @Benchmark
    public void wordSimpleComparator_measureNegativeCaseExecution(final Blackhole blackhole, final NegativeCase negativeCase) {
        blackhole.consume(wordSimpleComparator.areDifferentByOneLetter(negativeCase.word1, negativeCase.word2));
    }


    public static void main(final String[] args) throws Exception {
        final Options opt = new OptionsBuilder()
                .include(WordComparatorBenchmark.class.getSimpleName())
//                .addProfiler(StackProfiler.class)
//                .addProfiler(GCProfiler.class)
                .build();
        new Runner(opt).run();
    }
}
