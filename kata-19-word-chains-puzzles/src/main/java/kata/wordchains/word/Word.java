package kata.wordchains.word;

import lombok.Getter;

import java.util.Map;
import java.util.function.BinaryOperator;

import static java.util.Collections.unmodifiableMap;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.reducing;

@Getter
public final class Word {
    private final String value;
    private final Map<Character, Byte> chars;

    private long sumOfChars;
    private int maxCharValue;

    public Word(final String value) {
        this.value = value;
        this.chars = unmodifiableMap(getCharactersCount(value));

        for (final Map.Entry<Character, Byte> charOccurrence : chars.entrySet()) {
            this.sumOfChars += ((int) charOccurrence.getKey()) * charOccurrence.getValue();
            if (charOccurrence.getKey() > this.maxCharValue) {
                this.maxCharValue = charOccurrence.getKey();
            }
        }
    }

    private Map<Character, Byte> getCharactersCount(final String word) {
        return word.chars()
                .boxed()
                .collect(groupingBy(val -> (char) val.intValue(), reducing((byte) 0, e -> (byte) 1, sumBytes())));
    }

    private static BinaryOperator<Byte> sumBytes() {
        return (v1, v2) -> (byte) (v1 + v2);
    }
}
