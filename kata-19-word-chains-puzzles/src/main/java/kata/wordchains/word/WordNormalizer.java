package kata.wordchains.word;

public class WordNormalizer {
    public String normalize(final String word) {
        return word.trim().toLowerCase();
    }
}
