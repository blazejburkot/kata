package kata.wordchains.word.comparator;

public class WordEfficientComparator implements WordComparator {

    @Override
    public boolean areDifferentByOneLetter(final String word1, final String word2) {
        if (word1.length() == word2.length()) {
            return areWordWithTheSameLengthDifferentByOneLetter(word1, word2);
        } else if (Math.abs(word1.length() - word2.length()) == 1) {
            return areDifferentByOneLetterCalledWhenWordsAreDifferentByOneLetter(word1, word2);
        }
        return false;
    }

    @Override
    public boolean areEqual(final String word1, final String word2) {
        return word1.equals(word2);
    }

    private boolean areDifferentByOneLetterCalledWhenWordsAreDifferentByOneLetter(final String word1, final String word2) {
        final boolean isWord1Shorter = word1.length() < word2.length();
        final char[] charsOfShorterWord = isWord1Shorter ? word1.toCharArray() : word2.toCharArray();
        final char[] charsOfLongerWord = isWord1Shorter ? word2.toCharArray() : word1.toCharArray();

        int shorterWordIdx = 0;
        int longerWordIdx = 0;
        while (shorterWordIdx < charsOfShorterWord.length && charsOfShorterWord[shorterWordIdx] == charsOfLongerWord[longerWordIdx]) {
            ++shorterWordIdx;
            ++longerWordIdx;
        }

        if (shorterWordIdx == charsOfShorterWord.length) { // true when words are different on last position
            return true;
        }

        ++longerWordIdx;
        while (shorterWordIdx < charsOfShorterWord.length && charsOfShorterWord[shorterWordIdx] == charsOfLongerWord[longerWordIdx]) {
            ++shorterWordIdx;
            ++longerWordIdx;
        }

        return shorterWordIdx == charsOfShorterWord.length && longerWordIdx == charsOfLongerWord.length;
    }

    private boolean areWordWithTheSameLengthDifferentByOneLetter(final String word1, final String word2) {
        final char[] chars1 = word1.toCharArray();
        final char[] chars2 = word2.toCharArray();
        int diff = 0;
        for (int idx = 0; idx < chars1.length; ++idx) {
            if (chars1[idx] != chars2[idx]
                    && ++diff > 1) {
                return false;
            }
        }
        return diff == 1;
    }
}
