package kata.wordchains.word.comparator;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

public class CachingWordComparator implements WordComparator {
    private final WordComparator wordComparator;
    private final Map<WordsPair, Boolean> cache;
    @Getter
    private int cacheHit = 0;
    @Getter
    private int cacheMiss = 0;

    public CachingWordComparator(final WordComparator wordComparator, final int cacheInitialCapacity) {
        this.wordComparator = wordComparator;
        this.cache = new HashMap<>(cacheInitialCapacity);
    }

    @Override
    public boolean areDifferentByOneLetter(final String word1, final String word2) {
        final WordsPair wordsPair = new WordsPair(word1, word2);
        final Boolean cachedResult = cache.get(wordsPair);
        if (cachedResult != null) {
            ++cacheHit;
            return cachedResult;
        }

        ++cacheMiss;
        final boolean result = wordComparator.areDifferentByOneLetter(word1, word2);
        cache.put(wordsPair, result);
        return result;
    }

    @Override
    public boolean areEqual(final String word1, final String word2) {
        return wordComparator.areEqual(word1, word2);
    }

    public double countHitRatio() {
        final double cacheRequests = cacheMiss + cacheHit;
        return cacheRequests == 0 ? -1.0 : cacheHit / cacheRequests;
    }

    @AllArgsConstructor
    private static class WordsPair {
        private final String word1;
        private final String word2;

        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            if (!(o instanceof WordsPair)) {
                return false;
            }
            final WordsPair wordsPair = (WordsPair) o;
            return word1.equals(wordsPair.word1) && word2.equals(wordsPair.word2)
                    || word1.equals(wordsPair.word2) && word2.equals(wordsPair.word1);
        }

        @Override
        public int hashCode() {
            return (word1.hashCode() + word2.hashCode()) / 2;
        }
    }
}
