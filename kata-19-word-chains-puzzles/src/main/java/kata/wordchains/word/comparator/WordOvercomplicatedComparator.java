package kata.wordchains.word.comparator;

import org.apache.commons.text.similarity.LevenshteinDistance;

import java.util.HashMap;
import java.util.Objects;
import java.util.function.BiFunction;

public class WordOvercomplicatedComparator implements WordComparator{
    private final LevenshteinDistance simplifiedLevenshteinDistance;

    public WordOvercomplicatedComparator() {
        this.simplifiedLevenshteinDistance = new LevenshteinDistance(1);
    }

    @Override
    public boolean areDifferentByOneLetter(final String word1, final String word2) {
        final int sum = getNumberOfDifferentChars(word1, word2);
        final int acceptableDifference = word1.length() == word2.length() ? 2 : 1;
        if (sum > acceptableDifference) {
            return false;
        }

        return simplifiedLevenshteinDistance.apply(word1, word2) == 1;
    }

    private int getNumberOfDifferentChars(final String word1, final String word2) {
        final HashMap<Character, Integer> differentChars = new HashMap<>();
        for (int charIdx = 0; charIdx < word1.length(); ++charIdx) {
            char character = word1.charAt(charIdx);
            differentChars.compute(character, computeCharacterValue(1));
        }
        for (int charIdx = 0; charIdx < word2.length(); ++charIdx) {
            char character = word2.charAt(charIdx);
            differentChars.compute(character, computeCharacterValue(-1));
        }
        int differentCharsValue = 0;
        for (Integer value : differentChars.values()) {
            differentCharsValue += Math.abs(value);
        }
        return differentCharsValue;
    }

    private BiFunction<Character, Integer, Integer> computeCharacterValue(final int additionalValue) {
        return (key, currentValue) -> (currentValue == null ? 0 : currentValue) + additionalValue;
    }

    @Override
    public boolean areEqual(final String word1, final String word2) {
        return Objects.equals(word1, word2);
    }
}
