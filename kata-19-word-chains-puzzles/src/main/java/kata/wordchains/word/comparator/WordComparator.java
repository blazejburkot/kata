package kata.wordchains.word.comparator;

public interface WordComparator {

    boolean areDifferentByOneLetter(final String word1, final String word2);

    boolean areEqual(final String word1, final String word2);

}
