package kata.wordchains.word.comparator;

import org.apache.commons.text.similarity.LevenshteinDistance;

import java.util.Objects;

public class WordSimpleComparator implements WordComparator {
    private final LevenshteinDistance simplifiedLevenshteinDistance;

    public WordSimpleComparator() {
        this.simplifiedLevenshteinDistance = new LevenshteinDistance(1);
    }

    @Override
    public boolean areDifferentByOneLetter(final String word1, final String word2) {
        return simplifiedLevenshteinDistance.apply(word1, word2) == 1;
    }

    @Override
    public boolean areEqual(final String word1, final String word2) {
        return Objects.equals(word1, word2);
    }

}
