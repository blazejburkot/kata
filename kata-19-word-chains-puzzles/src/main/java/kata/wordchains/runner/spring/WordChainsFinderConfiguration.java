package kata.wordchains.runner.spring;

import kata.wordchains.dictionary.Dictionary;
import kata.wordchains.dictionary.DictionaryInitializer;
import kata.wordchains.finder.WordChainsSolutionFinder;
import kata.wordchains.word.WordNormalizer;
import kata.wordchains.word.comparator.WordComparator;
import kata.wordchains.word.comparator.WordEfficientComparator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;

import javax.annotation.PostConstruct;

@Configuration
@PropertySource("classpath:/application.properties")
@Import(DictionaryConfiguration.class)
public class WordChainsFinderConfiguration {

    @Autowired
    private DictionaryInitializer dictionaryInitializer;

    @PostConstruct
    public void init() {
        dictionaryInitializer.initialize();
    }

    @Bean
    public WordChainsSolutionFinder wordChainsFinder(final WordComparator comparator, final Dictionary dictionary) {
        return new WordChainsSolutionFinder(comparator, dictionary);
    }

    @Bean
    public WordNormalizer wordNormalizer() {
        return new WordNormalizer();
    }

    @Bean
    public WordComparator wordComparator() {
        return new WordEfficientComparator();
    }
}
