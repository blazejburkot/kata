package kata.wordchains.runner.spring;

import kata.wordchains.finder.AbstractWordChainsFinder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.builder.SpringApplicationBuilder;

public class SpringCommandLineRunner implements ApplicationRunner {
    private static final Logger LOGGER = LoggerFactory.getLogger(SpringCommandLineRunner.class);

    @Autowired
    private AbstractWordChainsFinder solutionFinder;

    public static void main(final String[] args) { // example args: gold lead
        new SpringApplicationBuilder()
                .sources(SpringCommandLineRunner.class, WordChainsFinderConfiguration.class)
                .run(args);
    }

    @Override
    public void run(final ApplicationArguments args) {
        LOGGER.info("Your application started with option names : {}", args.getOptionNames());

        if (args.getNonOptionArgs().size() != 2) {
            LOGGER.info("Application VM arguments: {}", String.join(" ", args.getSourceArgs()));
            System.err.println("Usage: java -jar file.jar <first word> <second word>");
            return;
        }

        final String firstWord = args.getNonOptionArgs().get(0);
        final String lastWord = args.getNonOptionArgs().get(1);
        solutionFinder.findAndPrintSolution(firstWord, lastWord);
    }
}
