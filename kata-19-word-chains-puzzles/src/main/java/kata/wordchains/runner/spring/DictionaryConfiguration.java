package kata.wordchains.runner.spring;

import kata.wordchains.reader.ResourceReader;
import kata.wordchains.reader.SpringResourceReader;
import kata.wordchains.dictionary.Dictionary;
import kata.wordchains.dictionary.DictionaryImpl;
import kata.wordchains.dictionary.DictionaryInitializer;
import kata.wordchains.word.WordNormalizer;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ResourceLoader;

@Configuration
public class DictionaryConfiguration {

    @Bean
    public ResourceReader resourceReader(final ResourceLoader resourceLoader) {
        return new SpringResourceReader(resourceLoader);
    }

    @Bean
    public DictionaryInitializer dictionaryInitializer(@Value("${word-chains.words-location}") final String wordsLocation,
                                                       final Dictionary dictionary,
                                                       final WordNormalizer wordNormalizer,
                                                       final ResourceReader resourceReader) {
        return new DictionaryInitializer(wordsLocation, dictionary, wordNormalizer, resourceReader);
    }

    @Bean
    public Dictionary dictionary() {
        return new DictionaryImpl();
    }
}
