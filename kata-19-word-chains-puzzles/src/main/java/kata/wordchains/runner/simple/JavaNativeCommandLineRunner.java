package kata.wordchains.runner.simple;

import kata.wordchains.finder.AbstractWordChainsFinder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

@SuppressWarnings("checkstyle:hideutilityclassconstructor")
public class JavaNativeCommandLineRunner {
    private static final Logger LOGGER = LoggerFactory.getLogger(JavaNativeCommandLineRunner.class);

    public static void main(final String[] args) {  // example args: gold lead
        LOGGER.info("Your application started with args: {}", Arrays.toString(args));

        if (args.length != 2) {
            System.err.println("Usage: java -jar file.jar <first word> <second word>");
            return;
        }

        final JavaNativeContextConfiguration configuration = new JavaNativeContextConfiguration();
        configuration.init();

        final AbstractWordChainsFinder solutionFinder = configuration.getSolutionFinder();
        final String firstWord = args[0];
        final String lastWord = args[1];
        solutionFinder.findAndPrintSolution(firstWord, lastWord);
    }
}
