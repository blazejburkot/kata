package kata.wordchains.runner.simple;

import kata.common.JavaNioResourceReader;
import kata.wordchains.dictionary.Dictionary;
import kata.wordchains.dictionary.DictionaryImpl;
import kata.wordchains.dictionary.DictionaryInitializer;
import kata.wordchains.finder.WordChainsSolutionFinder;
import kata.wordchains.reader.ResourceReader;
import kata.wordchains.word.WordNormalizer;
import kata.wordchains.word.comparator.WordEfficientComparator;
import lombok.Getter;

public class JavaNativeContextConfiguration {
//    private static final String WORDS_LOCATION = "word-list-slice.txt";
    private static final String WORDS_LOCATION = "word-list-data.txt";

    @Getter
    private final WordChainsSolutionFinder solutionFinder;
    private final DictionaryInitializer initializer;

    public JavaNativeContextConfiguration() {
        final ResourceReader resourceReader = new JavaNioResourceReader()::readLines;
        final Dictionary dictionary = new DictionaryImpl();
        final WordNormalizer normalizer = new WordNormalizer();
        this.initializer = new DictionaryInitializer(WORDS_LOCATION, dictionary, normalizer, resourceReader);
        final WordEfficientComparator comparator = new WordEfficientComparator();
        this.solutionFinder = new WordChainsSolutionFinder(comparator, dictionary);
    }

    public void init() {
        initializer.initialize();
    }
}
