package kata.wordchains.finder;

import kata.wordchains.dictionary.Dictionary;
import kata.wordchains.finder.helper.TaskResult;
import kata.wordchains.finder.helper.TreeElement;
import kata.wordchains.finder.helper.TreeRoot;
import kata.wordchains.word.comparator.WordComparator;
import lombok.Getter;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

@Getter
public class WordChainsSolutionFinder extends AbstractWordChainsFinder {
    private final Dictionary dictionary;
    private final WordComparator comparator;

    public WordChainsSolutionFinder(final WordComparator comparator, final Dictionary dictionary) {
        this.comparator = comparator;
        this.dictionary = dictionary;
    }

    @Override
    protected TaskResult findSolution(final String firstWord, final String lastWord) {
        final LinkedList<String> candidatesForNextElement = getCandidatesForNextElement(firstWord);
        final LinkedList<TreeElement<String>> wordsToProcessQueue = new LinkedList<>();
        wordsToProcessQueue.add(TreeRoot.with(firstWord));

        while (!wordsToProcessQueue.isEmpty()) {
            final TreeElement<String> processingElement = wordsToProcessQueue.poll();
            final String processingElementValue = processingElement.getValue();

            if (comparator.areDifferentByOneLetter(processingElementValue, lastWord)) {
                final List<String> solution = processingElement.addNode(lastWord).getPath();
                return new TaskResult(firstWord, lastWord, solution);
            }

            final Iterator<String> candidate = candidatesForNextElement.iterator();
            while (candidate.hasNext()) {
                final String candidateValue = candidate.next();
                if (comparator.areDifferentByOneLetter(processingElementValue, candidateValue)) {
                    candidate.remove();
                    wordsToProcessQueue.add(processingElement.addNode(candidateValue));
                }
            }
        }
        throw new IllegalArgumentException("Cannot find word chains for these arguments");
    }

    private LinkedList<String> getCandidatesForNextElement(final String firstWord) {
        return new LinkedList<>(dictionary.getWordsWithLength(firstWord.length()));
    }
}
