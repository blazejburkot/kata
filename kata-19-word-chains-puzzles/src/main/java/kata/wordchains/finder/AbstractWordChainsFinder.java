package kata.wordchains.finder;

import kata.wordchains.dictionary.Dictionary;
import kata.wordchains.finder.helper.TaskResult;
import kata.wordchains.runner.simple.JavaNativeCommandLineRunner;
import kata.wordchains.word.comparator.WordComparator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StopWatch;

import java.util.Arrays;

public abstract class AbstractWordChainsFinder {
    private static final Logger LOGGER = LoggerFactory.getLogger(JavaNativeCommandLineRunner.class);

    public final void findAndPrintSolution(final String firstWord, final String lastWord) {
        final StopWatch watch = new StopWatch();
        watch.start();
        try {
            final TaskResult wordChains = findWordChains(firstWord, lastWord);
            System.out.println("Word chains: " + String.join(" -> ", wordChains.getWordChains()));
        } catch (final IllegalArgumentException ex) {
            System.err.println(ex.getMessage());
        } catch (final Exception ex) {
            System.err.println("Exception occurs on processing: " + ex.getMessage());
            ex.printStackTrace();
        } finally {
            watch.stop();
            LOGGER.info("Result was found in {} ms", watch.getTotalTimeMillis());
        }
    }

    public final TaskResult findWordChains(final String firstWord, final String lastWord) {
        assertWordIsFromDictionary(firstWord);
        assertWordIsFromDictionary(lastWord);

        if (firstWord.length() != lastWord.length()) {
            throw new IllegalArgumentException("The words are of different sizes");
        }

        if (getComparator().areEqual(firstWord, lastWord)) {
            throw new IllegalArgumentException("The Words are the same");
        }

        if (getComparator().areDifferentByOneLetter(firstWord, lastWord)) {
            return new TaskResult(firstWord, lastWord, Arrays.asList(firstWord, lastWord));
        }

        return findSolution(firstWord, lastWord);
    }

    protected abstract TaskResult findSolution(final String firstWord, final String secondWord);

    private void assertWordIsFromDictionary(final String word) {
        if (!getDictionary().contains(word)) {
            throw new IllegalArgumentException("Dictionary does not contain the word: " + word);
        }
    }

    protected abstract Dictionary getDictionary();
    protected abstract WordComparator getComparator();
}
