package kata.wordchains.finder.helper;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@Getter
@AllArgsConstructor
public class TaskResult {
    private final String firstWord;
    private final String secondWord;
    private final List<String> wordChains;
}
