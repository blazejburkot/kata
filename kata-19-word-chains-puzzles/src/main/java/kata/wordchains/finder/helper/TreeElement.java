package kata.wordchains.finder.helper;

import lombok.Getter;
import lombok.ToString;

import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@ToString(of = {"value", "depth"})
public class TreeElement<T> {
    @Getter
    private final T value;
    private final int depth;
    private final TreeElement<T> parent;

    TreeElement(final TreeElement<T> parent, final T value, final int depth) {
        this.parent = parent;
        this.value = value;
        this.depth = depth;
    }

    public TreeElement<T> addNode(final T nodeValue) {
        Assert.notNull(nodeValue, "NodeValue cannot be null");
        final TreeElement<T> node = new TreeElement<>(this, nodeValue, depth + 1);
        return node;
    }

    public List<T> getPath() {
        final List<T> wordChainsResult = new ArrayList<>(depth);
        TreeElement<T> element = this;
        while (element != null) {
            wordChainsResult.add(element.getValue());
            element = element.parent;
        }
        Collections.reverse(wordChainsResult);
        return wordChainsResult;
    }
}
