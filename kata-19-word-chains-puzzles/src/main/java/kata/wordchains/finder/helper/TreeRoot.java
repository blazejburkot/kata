package kata.wordchains.finder.helper;

public final class TreeRoot<T> extends TreeElement<T> {

    private TreeRoot(final T value) {
        super(null, value, 0);
    }

    public static <T> TreeRoot<T> with(final T value) {
        return new TreeRoot<>(value);
    }
}
