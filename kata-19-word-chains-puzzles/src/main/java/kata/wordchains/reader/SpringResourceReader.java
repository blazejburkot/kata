package kata.wordchains.reader;

import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UncheckedIOException;
import java.nio.charset.Charset;
import java.util.stream.Stream;

public class SpringResourceReader implements ResourceReader {
    private final ResourceLoader resourceLoader;

    public SpringResourceReader(final ResourceLoader resourceLoader) {
        this.resourceLoader = resourceLoader;
    }

    public Stream<String> readLines(final String resourceLocation, final Charset resourceEncoding) {
        try {
            final Resource resource = resourceLoader.getResource(resourceLocation);
            final BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(resource.getInputStream(), resourceEncoding));
            return bufferedReader.lines();
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }
}
