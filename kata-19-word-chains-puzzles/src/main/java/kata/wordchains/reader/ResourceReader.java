package kata.wordchains.reader;

import java.nio.charset.Charset;
import java.util.stream.Stream;

public interface ResourceReader {
    Stream<String> readLines(final String resourceLocation, final Charset resourceEncoding);
}
