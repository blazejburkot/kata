package kata.wordchains.dictionary;

import kata.wordchains.reader.ResourceReader;
import kata.wordchains.word.WordNormalizer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StopWatch;

import java.nio.charset.StandardCharsets;

public class DictionaryInitializer {
    private static final Logger LOGGER = LoggerFactory.getLogger(DictionaryInitializer.class);

    private final String wordsLocation;
    private final Dictionary dictionary;
    private final WordNormalizer wordNormalizer;
    private final ResourceReader resourceReader;

    private boolean initialized;

    public DictionaryInitializer(final String wordsLocation,
                                 final Dictionary dictionary,
                                 final WordNormalizer wordNormalizer,
                                 final ResourceReader resourceReader) {
        this.wordsLocation = wordsLocation;
        this.dictionary = dictionary;
        this.wordNormalizer = wordNormalizer;
        this.resourceReader = resourceReader;
    }

    public synchronized void initialize() {
        if (initialized) {
            throw new IllegalStateException("Dictionary has been already initialized");
        }

        final StopWatch watch = new StopWatch();
        watch.start();
        LOGGER.debug("Start reading words from {}", wordsLocation);

        resourceReader.readLines(wordsLocation, StandardCharsets.UTF_8)
                .map(wordNormalizer::normalize)
                .forEach(dictionary::addWord);
        initialized = true;

        watch.stop();
        LOGGER.info("Dictionary was loaded in {} ms. Number of words {}", watch.getTotalTimeMillis(), dictionary.size());
    }
}
