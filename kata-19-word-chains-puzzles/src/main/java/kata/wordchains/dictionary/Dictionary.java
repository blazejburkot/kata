package kata.wordchains.dictionary;

import java.util.Collection;

public interface Dictionary {

    void addWord(final String word);

    long size();

    boolean contains(String word);

    Collection<String> getWordsWithLength(int length);

}
