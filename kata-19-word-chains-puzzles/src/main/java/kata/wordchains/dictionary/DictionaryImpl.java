package kata.wordchains.dictionary;

import org.springframework.util.StringUtils;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import static java.util.Arrays.stream;

public class DictionaryImpl implements Dictionary {
    private final Map<Integer, Collection<String>> words;

    public DictionaryImpl() {
        this.words = new HashMap<>();
    }

    public static DictionaryImpl of(final String ... words) {
        final DictionaryImpl dictionary = new DictionaryImpl();
        stream(words).forEach(dictionary::addWord);
        return dictionary;
    }

    @Override
    public Collection<String> getWordsWithLength(final int length) {
        return words.getOrDefault(length, Collections.emptySet());
    }

    @Override
    public boolean contains(final String word) {
        return words.getOrDefault(word.length(), Collections.emptySet()).contains(word);
    }

    @Override
    public void addWord(final String word) {
        assertNotEmptyString(word);
        words.computeIfAbsent(word.length(), ignored -> new LinkedList<>()).add(word);
    }

    @Override
    public long size() {
        return words.values().stream()
                .mapToLong(Collection::size).sum();
    }

    private void assertNotEmptyString(final String word) {
        if (StringUtils.isEmpty(word)) {
            throw new IllegalArgumentException("Given argument is empty string");
        }
    }
}
