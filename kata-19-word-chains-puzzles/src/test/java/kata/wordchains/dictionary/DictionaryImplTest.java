package kata.wordchains.dictionary;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class DictionaryImplTest {

    @Test
    public void simpleTest() {
        // given
        final DictionaryImpl dictionary = new DictionaryImpl();
        // when
        dictionary.addWord("dog");
        dictionary.addWord("cat");
        dictionary.addWord("horse");
        // then
        assertThat(dictionary.getWordsWithLength(3)).containsExactlyInAnyOrder("dog", "cat");
        assertThat(dictionary.getWordsWithLength(5)).containsExactlyInAnyOrder("horse");

        assertThat(dictionary.contains("cat")).isTrue();
        assertThat(dictionary.contains("pig")).isFalse();
    }

}