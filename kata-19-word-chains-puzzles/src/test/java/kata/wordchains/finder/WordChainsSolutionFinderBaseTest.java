package kata.wordchains.finder;

import kata.wordchains.finder.helper.TaskResult;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

public abstract class WordChainsSolutionFinderBaseTest {
    static final String[] WORDS = {"co", "cg", "coo", "cot", "cat", "cog", "dog", "doge", "done", "nope"};

    protected abstract AbstractWordChainsFinder solutionFinder();

    @Test
    public void shouldSolveTaskExample() {
        // when
        final TaskResult taskResult = solutionFinder().findWordChains("cat", "dog");
        // then
        assertThat(taskResult.getFirstWord()).isEqualTo("cat");
        assertThat(taskResult.getSecondWord()).isEqualTo("dog");
        assertThat(taskResult.getWordChains()).containsExactly("cat", "cot", "cog", "dog");
    }

//    @Test
//    public void shouldHandleWordsWithDifferentLength() {
//        // when
//        final TaskResult taskResult = solutionFinder().findWordChains("co", "done");
//        // then
//        assertThat(taskResult.getWordChains()).containsExactly("co", "cog", "dog", "doge", "done");
//    }

    @Test
    public void shouldReturnTheShortestWordChains() {
        // when
        final TaskResult taskResult = solutionFinder().findWordChains("coo", "cat");
        // then
        assertThat(taskResult.getWordChains()).containsExactly("coo", "cot", "cat"); // alt. worse solution 'coo > cog -> cot -> cat'
    }

    @Test
    public void shouldThrowIllegalArgumentExceptionWhenDictionaryDoesNotContainWord() {
        final String wordFromDictionary = "cat";
        final String wordNotFromDictionary = "Cat"; // findWordChains method should be case sensitive

        assertThatExceptionOfType(IllegalArgumentException.class)
                .isThrownBy(() -> solutionFinder().findWordChains(wordNotFromDictionary, wordFromDictionary))
                .withMessageContaining(wordNotFromDictionary );
    }

    @Test
    public void shouldThrowIllegalArgumentExceptionWhenCannotFindSolutionForGivenWords() {
        assertThatExceptionOfType(IllegalArgumentException.class)
                .isThrownBy(() -> solutionFinder().findWordChains("cat", "nope"));
    }

    @Test
    public void shouldThrowIllegalArgumentExceptionWhenWordsAreEqual() {
        assertThatExceptionOfType(IllegalArgumentException.class)
                .isThrownBy(() -> solutionFinder().findWordChains("cat", "cat"));
    }
}