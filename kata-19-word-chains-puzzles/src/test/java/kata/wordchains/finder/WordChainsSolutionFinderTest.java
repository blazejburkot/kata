package kata.wordchains.finder;

import kata.wordchains.dictionary.DictionaryImpl;
import kata.wordchains.word.comparator.WordComparator;
import kata.wordchains.word.comparator.WordEfficientComparator;

public class WordChainsSolutionFinderTest extends WordChainsSolutionFinderBaseTest {
    private DictionaryImpl dictionary = DictionaryImpl.of(WORDS);
    private WordComparator comparator = new WordEfficientComparator();
    private WordChainsSolutionFinder finder = new WordChainsSolutionFinder(comparator, dictionary);

    @Override
    protected AbstractWordChainsFinder solutionFinder() {
        return finder;
    }
}