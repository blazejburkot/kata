package kata.wordchains.word;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(Parameterized.class)
public class WordNormalizerParameterizedTest {

    @Parameterized.Parameters(name= "Test {index}: word={0}")
    public static Iterable<Object[]> data() {
        return Arrays.asList(new Object[][] {
                {"Cat", "cat"},
                {"cat", "cat"}
        });
    }

    @Parameterized.Parameter(0)
    public String word;

    @Parameterized.Parameter(1)
    public String expectedValue;

    private WordNormalizer normalizer = new WordNormalizer();

    @Test
    public void test() {
        assertThat(normalizer.normalize(word)).isEqualTo(expectedValue);
    }
}