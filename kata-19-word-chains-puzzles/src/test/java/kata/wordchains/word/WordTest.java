package kata.wordchains.word;

import com.google.common.collect.ImmutableMap;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class WordTest {

    @Test
    public void simpleTest() {
        // given
        final String txt = "abc";
        // when
        final Word word = new Word(txt);
        // then
        final Map<Character, Byte> expectedChars = singleCharOccurrence('a', 'b', 'c');
        assertThat(word.getChars()).containsAllEntriesOf(expectedChars);
        assertThat(word.getSumOfChars()).isEqualTo('a' + 'b' + 'c');
        assertThat(word.getMaxCharValue()).isEqualTo('c');
    }

    @Test
    public void shoudFindMultipleCharOccurence() {
        // given
        final String txt = "aaaaBBB";
        // when
        final Word word = new Word(txt);
        // then
        final Map<Character, Byte> expectedChars = ImmutableMap.of('a', (byte) 4, 'B', (byte) 3);
        assertThat(word.getChars()).containsAllEntriesOf(expectedChars);
        assertThat(word.getSumOfChars()).isEqualTo('a' * 4 + 'B' * 3);
        assertThat(word.getMaxCharValue()).isEqualTo('a');
    }

    @Test
    public void shouldSupportNonAsciiChars() {
        // given
        final String txt = "\u00e9a\u010db";
        // when
        final Word word = new Word(txt);
        // then
        final Map<Character, Byte> expectedChars = singleCharOccurrence('a', '\u00e9', 'b', '\u010d');
        assertThat(word.getChars()).containsAllEntriesOf(expectedChars);
        assertThat(word.getSumOfChars()).isEqualTo('a' + '\u00e9' + 'b' + '\u010d');
        assertThat(word.getMaxCharValue()).isEqualTo('\u010d');
    }

    private Map<Character, Byte> singleCharOccurrence(final char ... chars) {
        final Map<Character, Byte> map = new HashMap<>();
        for (char c : chars) {
            map.put(c, (byte) 1);
        }
        return map;
    }
}