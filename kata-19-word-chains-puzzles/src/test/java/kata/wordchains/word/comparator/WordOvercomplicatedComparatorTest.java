package kata.wordchains.word.comparator;

public class WordOvercomplicatedComparatorTest extends WordComparatorBaseTest {

    @Override
    public WordComparator getWordComparator() {
        return new WordOvercomplicatedComparator();
    }
}