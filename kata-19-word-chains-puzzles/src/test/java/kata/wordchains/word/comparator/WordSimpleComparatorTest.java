package kata.wordchains.word.comparator;

public class WordSimpleComparatorTest extends WordComparatorBaseTest {

    @Override
    public WordComparator getWordComparator() {
        return new WordSimpleComparator();
    }
}