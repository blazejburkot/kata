package kata.wordchains.word.comparator;

public class WordEfficientComparatorTest extends WordComparatorBaseTest {

    @Override
    public WordComparator getWordComparator() {
        return new WordEfficientComparator();
    }
}