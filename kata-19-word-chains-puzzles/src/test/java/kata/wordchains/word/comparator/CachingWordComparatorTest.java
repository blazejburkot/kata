package kata.wordchains.word.comparator;

import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class CachingWordComparatorTest {
    private CachingWordComparator comparator;
    private WordComparator proxiedWordComparator;

    @Before
    public void setUp() {
        this.proxiedWordComparator = mock(WordComparator.class);
        this.comparator = new CachingWordComparator(proxiedWordComparator, 10);
    }

    @Test
    public void simpleTest() {
        // given
        final String word1 = "abcde";
        final String word2 = "abcdd";
        when(proxiedWordComparator.areDifferentByOneLetter(any(), any())).thenReturn(Boolean.FALSE);
        when(proxiedWordComparator.areDifferentByOneLetter(word1, word2)).thenReturn(Boolean.TRUE);
        // when
        final boolean result1 = comparator.areDifferentByOneLetter(word1, word2);
        final boolean result2 = comparator.areDifferentByOneLetter(word1, word2);
        final boolean result3 = comparator.areDifferentByOneLetter(word2, word1);
        final boolean result4 = comparator.areDifferentByOneLetter(word1, word2);
        final boolean result5 = comparator.areDifferentByOneLetter(word1, word1);
        // then
        assertThat(result1)
                .isEqualTo(result2)
                .isEqualTo(result3)
                .isEqualTo(result4)
                .isNotEqualTo(result5);
        assertThat(comparator.getCacheHit()).isEqualTo(3);
        assertThat(comparator.getCacheMiss()).isEqualTo(2);
        verify(proxiedWordComparator, times(2));
    }
}