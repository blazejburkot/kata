package kata.wordchains.word.comparator;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(Parameterized.class)
public abstract class WordComparatorBaseTest {

    @Parameterized.Parameters(name= "Test {index}: word1={0}, word2={1}")
    public static Iterable<Object[]> data() {
        return Arrays.asList(new Object[][] {
                {"abcde", "abcde", false, true},
                {"abcde", "abcdd", true, false},
                {"abcde", "abccc", false, false},
                {"abcde", "abbbb", false, false},
                {"\u00e9trang\u010dres", "\u00e9trang\u010drs", true, false},
                {"\u00e9trang\u010dres", "etrang\u010dres", true, false},
                {"\u00e9trang\u010dres", "\u00e9trang\u010dres", false, true},
                {"\u00e9trang\u010dres", "etrang\u010dres", true, false},
                {"\u00e9trang\u010dres", "etrang\u010drs", false, false},

                {"abc", "bbc", true, false},
                {"abc", "bc", true, false},
                {"abc", "ac", true, false},
                {"abc", "ab", true, false},
        });
    }

    @Parameterized.Parameter(0)
    public String word1;

    @Parameterized.Parameter(1)
    public String word2;

    @Parameterized.Parameter(2)
    public boolean areDifferentByOneLetter;

    @Parameterized.Parameter(3)
    public boolean areEqual;

    @Test
    public void test() {
        assertThat(getWordComparator().areDifferentByOneLetter(word1, word2)).isEqualTo(areDifferentByOneLetter);
        assertThat(getWordComparator().areEqual(word1, word2)).isEqualTo(areEqual);
    }

    public abstract WordComparator getWordComparator();
}