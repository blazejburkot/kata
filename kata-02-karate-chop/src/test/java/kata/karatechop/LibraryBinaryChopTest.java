package kata.karatechop;

public class LibraryBinaryChopTest extends BinaryChopBaseTest {

    @Override
    protected BinaryChop getBinaryChop() {
        return new LibraryBinaryChop();
    }
}