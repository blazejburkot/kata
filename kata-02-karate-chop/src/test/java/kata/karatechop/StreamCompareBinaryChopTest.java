package kata.karatechop;

public class StreamCompareBinaryChopTest extends BinaryChopBaseTest {

    @Override
    protected BinaryChop getBinaryChop() {
        return new StreamCompareBinaryChop();
    }
}