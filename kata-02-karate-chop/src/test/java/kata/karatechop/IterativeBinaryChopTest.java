package kata.karatechop;

public class IterativeBinaryChopTest extends BinaryChopBaseTest {

    @Override
    protected BinaryChop getBinaryChop() {
        return new IterativeBinaryChop();
    }
}