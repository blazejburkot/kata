package kata.karatechop;

public class LoopCompareBinaryChopTest extends BinaryChopBaseTest {

    @Override
    protected BinaryChop getBinaryChop() {
        return new LoopCompareBinaryChop();
    }
}