package kata.karatechop;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(Parameterized.class)
public abstract class BinaryChopBaseTest {

    @Parameterized.Parameters(name= "Test {index}: key={0}, array={1}, expectedIdx={2}")
    public static Iterable<Object[]> data() {
        final List<Object[]> testData = new ArrayList<>();

        addTestCase(testData, 0, array(), -1);
        addTestCase(testData, 0, array(1), -1);
        addTestCase(testData, 1, array(1), 0);
        addTestCase(testData, 2, array(1), -1);
        addTestCase(testData, 0, array(1, 3, 5), -1);
        addTestCase(testData, 1, array(1, 3, 5), 0);
        addTestCase(testData, 2, array(1, 3, 5), -1);
        addTestCase(testData, 3, array(1, 3, 5), 1);
        addTestCase(testData, 4, array(1, 3, 5), -1);
        addTestCase(testData, 5, array(1, 3, 5), 2);
        addTestCase(testData, 6, array(1, 3, 5), -1);
        addTestCase(testData, 0, array(1, 3, 5, 7), -1);
        addTestCase(testData, 1, array(1, 3, 5, 7), 0);
        addTestCase(testData, 2, array(1, 3, 5, 7), -1);
        addTestCase(testData, 3, array(1, 3, 5, 7), 1);
        addTestCase(testData, 4, array(1, 3, 5, 7), -1);
        addTestCase(testData, 5, array(1, 3, 5, 7), 2);
        addTestCase(testData, 6, array(1, 3, 5, 7), -1);
        addTestCase(testData, 7, array(1, 3, 5, 7), 3);
        addTestCase(testData, 8, array(1, 3, 5, 7), -1);
        addTestCase(testData, 1, array(1, 1, 1, 3, 5, 7), 0, 1, 2);
        addTestCase(testData, 3, array(1, 1, 1, 3, 5, 7), 3);
        addTestCase(testData, 4, array(1, 1, 1, 3, 5, 7), -1);
        addTestCase(testData, 3, array(1, 1, 3, 3, 5, 7), 2, 3);

        return testData;
    }

    @Parameterized.Parameter(0)
    public int key;

    @Parameterized.Parameter(1)
    public int[] array;

    @Parameterized.Parameter(2)
    public int[] expectedIndexes;

    @Test
    public void test() {
        // when
        final int keyIndex = getBinaryChop().chop(key, array);
        // then
        assertThat(expectedIndexes).contains(keyIndex);
    }


    protected abstract BinaryChop getBinaryChop();


    private static int[] array(final int ... array) {
        return array;
    }

    private static void addTestCase(final List<Object[]> testData, final int key, final int[] array, final int ... expectedIndexes) {
        testData.add(new Object[]{key, array, expectedIndexes});
    }
}