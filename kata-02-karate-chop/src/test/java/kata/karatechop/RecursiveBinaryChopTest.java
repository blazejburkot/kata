package kata.karatechop;

public class RecursiveBinaryChopTest extends BinaryChopBaseTest {

    @Override
    protected BinaryChop getBinaryChop() {
        return new RecursiveBinaryChop();
    }
}