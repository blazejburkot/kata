package kata.karatechop;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Warmup;
import org.openjdk.jmh.infra.Blackhole;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.util.concurrent.TimeUnit;

/*
 * You can run this benchmark:
 *
 * a) Via the command line:
 *    $ mvn -Pbenchmarks clean package
 *    $ java -jar target/benchmarks.jar kata.karatechop.BinaryChopBenchmark
 *
 * b) Via the Java API:
 *    just run main method
 */
@Fork(2)
@Warmup(iterations = 1, time = 1)
@Measurement(iterations = 10, time = 1)
@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
public class BinaryChopBenchmark {
    private static IterativeBinaryChop iterativeBinaryChop = new IterativeBinaryChop();
    private static LibraryBinaryChop libraryBinaryChop = new LibraryBinaryChop();
    private static LoopCompareBinaryChop loopCompareBinaryChop = new LoopCompareBinaryChop();
    private static RecursiveBinaryChop recursiveBinaryChop = new RecursiveBinaryChop();
    private static StreamCompareBinaryChop streamCompareBinaryChop = new StreamCompareBinaryChop();

    @State(Scope.Benchmark)
    public static class TestData {
        volatile int[] array = {0, 1, 1, 1, 3, 3, 4, 4, 5, 7, 9, 9, 10, 11, 13, 14, 14, 14, 16, 16, 17, 19, 21, 21, 21, 22, 23, 23, 24, 26,
                26, 29, 30, 30, 32, 32, 32, 34, 34, 34, 37, 39, 40, 42, 44, 45, 46, 46, 47, 49};
        volatile int lowestValue = 0;
        volatile int averageValue = 23;
        volatile int highestValue = 49;
    }


    @Benchmark
    public void measure_iterativeBinaryChop(final Blackhole blackhole, final TestData data) {
        blackhole.consume(iterativeBinaryChop.chop(data.lowestValue, data.array));
        blackhole.consume(iterativeBinaryChop.chop(data.averageValue, data.array));
        blackhole.consume(iterativeBinaryChop.chop(data.highestValue, data.array));
    }

    @Benchmark
    public void measure_libraryBinaryChop(final Blackhole blackhole, final TestData data) {
        blackhole.consume(libraryBinaryChop.chop(data.lowestValue, data.array));
        blackhole.consume(libraryBinaryChop.chop(data.averageValue, data.array));
        blackhole.consume(libraryBinaryChop.chop(data.highestValue, data.array));
    }

    @Benchmark
    public void measure_loopCompareBinaryChop(final Blackhole blackhole, final TestData data) {
        blackhole.consume(loopCompareBinaryChop.chop(data.lowestValue, data.array));
        blackhole.consume(loopCompareBinaryChop.chop(data.averageValue, data.array));
        blackhole.consume(loopCompareBinaryChop.chop(data.highestValue, data.array));
    }

    @Benchmark
    public void measure_recursiveBinaryChop(final Blackhole blackhole, final TestData data) {
        blackhole.consume(recursiveBinaryChop.chop(data.lowestValue, data.array));
        blackhole.consume(recursiveBinaryChop.chop(data.averageValue, data.array));
        blackhole.consume(recursiveBinaryChop.chop(data.highestValue, data.array));
    }

    @Benchmark
    public void measure_streamCompareBinaryChop(final Blackhole blackhole, final TestData data) {
        blackhole.consume(streamCompareBinaryChop.chop(data.lowestValue, data.array));
        blackhole.consume(streamCompareBinaryChop.chop(data.averageValue, data.array));
        blackhole.consume(streamCompareBinaryChop.chop(data.highestValue, data.array));
    }


    public static void main(final String[] args) throws Exception {
        final Options opt = new OptionsBuilder()
                .include(BinaryChopBenchmark.class.getSimpleName())
                .build();
        new Runner(opt).run();
    }
}
