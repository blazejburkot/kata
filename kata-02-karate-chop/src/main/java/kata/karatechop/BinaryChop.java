package kata.karatechop;

public interface BinaryChop {

    int chop(final int key, final int[] array);
}
