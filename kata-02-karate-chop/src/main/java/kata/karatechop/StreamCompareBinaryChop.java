package kata.karatechop;

import java.util.stream.IntStream;

public class StreamCompareBinaryChop implements BinaryChop {

    @Override
    public int chop(final int key, final int[] array) {
        return IntStream.range(0, array.length)
//                .parallel()
                .filter(idx -> array[idx] == key)
                .findAny()
                .orElse(-1);
    }
}
