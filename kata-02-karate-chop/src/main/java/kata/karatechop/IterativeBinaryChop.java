package kata.karatechop;

public class IterativeBinaryChop implements BinaryChop {

    @Override
    public int chop(final int key, final int[] array) {
        int leftIdx = 0;
        int rightIdx = array.length - 1;

        while (leftIdx <= rightIdx) {
            final int midIdx = (rightIdx + leftIdx) / 2;
            final int midValue = array[midIdx];
            if (midValue < key) {
                leftIdx = midIdx + 1;
            } else if (midValue > key) {
                rightIdx = midIdx - 1;
            } else {
                return midIdx;
            }
        }
        return -1;
    }
}
