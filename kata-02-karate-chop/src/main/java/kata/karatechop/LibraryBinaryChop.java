package kata.karatechop;

import java.util.Arrays;

public class LibraryBinaryChop implements BinaryChop {

    @Override
    public int chop(final int key, final int[] array) {
        final int result = Arrays.binarySearch(array, key);
        return result >= 0 ? result : -1;
    }
}
