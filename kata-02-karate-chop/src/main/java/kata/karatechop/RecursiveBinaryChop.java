package kata.karatechop;

public class RecursiveBinaryChop implements BinaryChop {

    @Override
    public int chop(final int key, final int[] array) {
        if (array.length == 0) {
            return -1;
        }
        return chop(key, array, 0, array.length - 1);
    }

    private int chop(final int key, final int[] array, final int leftIdx, final int rightIdx) {
        if (leftIdx >= rightIdx) {
            return array[leftIdx] == key ? leftIdx : -1;
        }
        final int midIdx = (rightIdx + leftIdx) / 2;
        final int midValue = array[midIdx];
        if (midValue < key) {
            return chop(key, array, midIdx + 1, rightIdx);
        } else if (midValue > key) {
            return chop(key, array, leftIdx, midIdx - 1);
        } else {
            return midIdx;
        }
    }
}
