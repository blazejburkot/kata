package kata.karatechop;

public class LoopCompareBinaryChop implements BinaryChop {

    @Override
    public int chop(final int key, final int[] array) {
        for (int idx = 0; idx < array.length; ++idx) {
            if (array[idx] == key) {
                return idx;
            }
        }
        return -1;
    }
}
